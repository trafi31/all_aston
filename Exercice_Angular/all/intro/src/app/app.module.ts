import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CounterComponent } from './counter/counter.component';
import { SwitchComponent } from './tp-correction/switch/switch.component';
import { CarouselComponent } from './tp-correction/carousel/carousel.component';
import { VirtualkbComponent } from './tp-correction/virtualkb/virtualkb.component';
import { CommunicationModule } from './communication/communication.module';
import { JuvejerseyModule } from './juvejersey/juvejersey.module';
import { DemoComponent } from './demo/demo.component';
import { FruitModule } from './fruit/fruit.module';
import { PlayerManagerModule } from './player-manager/player-manager.module';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CantiComponent } from './canti/canti.component';


@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    SwitchComponent,
    CarouselComponent,
    VirtualkbComponent,
    DemoComponent,
    LoginComponent,
    CantiComponent
  ],
  imports: [
    BrowserModule,
    CommunicationModule,
    JuvejerseyModule,
    FruitModule,
    PlayerManagerModule,
    FormsModule, // permet d'utiliser la directive ngModel
    HttpClientModule // expose le service HttpClient
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
