import { Component, OnInit } from '@angular/core';
import { Article } from '../interfaces';

@Component({
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {
  articles: Article[] = [
    { name: "Paire de Nike", price: 120.50 },
    { name: "Tracteur de course", price: 570.00 },
    { name: "Formation Angular", price: 6300.00 }
  ];
  subtotals: number[] = [];
  total: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  updateSubtotals(event: any) {
    this.subtotals = event; // récupération des sous-totaux émis par article-list
    this.computeTotal();
  }

  private computeTotal() {
    this.total = 0;
    this.subtotals.forEach(subtotal => this.total += subtotal);
  }

}
