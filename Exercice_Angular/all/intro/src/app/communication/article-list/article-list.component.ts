import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Article } from '../interfaces';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent {
  @Input() articles: Article[] = [];
  @Output() subtotalsChange: EventEmitter<number[]> = new EventEmitter();
  private subtotals: number[] = [];

  constructor() { }

  onChange(event: any, price: number, index: number) {
    let qty = event.target.value;
    this.subtotals[index] = (qty * price);
    this.subtotalsChange.emit(this.subtotals);
  }

}
