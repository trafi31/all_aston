import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { POSITIONS, TEAMS, Team } from '../player';

@Component({
  selector: 'app-player-filter',
  templateUrl: './player-filter.component.html',
  styleUrls: ['./player-filter.component.css']
})
export class PlayerFilterComponent implements OnInit {
  options: string[] | Team[];
  @Input() key: string = "";
  @Output() onChange: EventEmitter<{}> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    if (this.key === "position") {
      this.options = POSITIONS;
    } else if (this.key === "teamid") {
      this.options = TEAMS;
    }
  }

  change(event: any) {
    let option = event.target.value;
    this.onChange.emit({ k: this.key, option })
  }

}
