import { Component, OnInit } from '@angular/core';
import { Fruit } from '../fruit';
import { FruitService } from '../fruit.service';

@Component({
  selector: 'fruit-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  fruits: Fruit[];
  //private fruitService: FruitService = new FruitService();
  private _fruitName: string = "";

  // Injection de dépendance
  constructor(private fruitService: FruitService) {
    //this.fruits = this.fruitService.getFruits();
  }

  ngOnInit(): void {
    //this.fruits.push(this.fruitService.getSuperFruit())
    this.fruitService.getFruitsAjax()
      .subscribe((fruits: Fruit[]) => {
        this.fruits = fruits;
      })
  }

  save() {
    // requête ajax
    let fruit: Fruit = {
      name: this._fruitName,
      like: 0
    }
    this.fruitService.postFruit(fruit)
      .subscribe((fruit: Fruit) => {
        this.fruits.push(fruit);
      });
  }

  onKeyup(event :any) {
    let value = event.target.value;

    // Todo: vérifier value
    this._fruitName = value;
  }

}
