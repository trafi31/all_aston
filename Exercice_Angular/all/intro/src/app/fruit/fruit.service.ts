import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Fruit } from './fruit';

const API = "http://localhost:3000/fruits";

@Injectable({
  providedIn: 'root'
})
export class FruitService {
  private fruits: string[] = ["cerise", "pomme", "kiwi"];

  // Injection du service HttpClient
  constructor(private http: HttpClient) { }

  getFruits(): string[] {
    return this.fruits;
  }

  getFruitsAjax() {
    // On retourne l'observable
    // Pas de subscribe à ce niveau
    return this.http.get(API);
  }

  getSuperFruit(): string {
    return "Grenade";
  }

  postFruit(fruit: Fruit) {
    return this.http.post(API, fruit);
  }
}
