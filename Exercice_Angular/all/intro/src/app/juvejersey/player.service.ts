import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private players: string[] = ["1-Buffon","7-Ronaldo","10-Dybala"];

  constructor() { }

  getPlayers(): string[] {
    return this.players;
  }
}
