import { Component, OnInit } from '@angular/core';
import { Players } from '../players';
import { PlayersService } from '../players.service';

@Component({
  selector: 'players-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  players: Players[];
  private _id: number = 0;
  private _firstname: string = "";
  private _lastname: string = "";
  private _teamid:number = 0;
  private _position: string = "";
  private _age: number = 0;
  
  constructor(private playerservice: PlayersService) {
    
  }

  ngOnInit(): void {

    this.playerservice.getPlayersAjax()
      .subscribe((players: Players[]) => {
        this.players = players;
      })
  }

  save() {
    let players: Players = {
      id: this._id,
      firstname: this._firstname,
      lastname: this._lastname,
      teamid: this._teamid,
      position: this._position,
      age: this._age
    }
    this.playerservice.postPlayers(players)
      .subscribe((players: Players) => {
        this.players.push(players);
      });
  }

  onKeyup(event :any) {
    let value = event.target.value;

    // Todo: vérifier value
    this._firstname = value;
  }

}
