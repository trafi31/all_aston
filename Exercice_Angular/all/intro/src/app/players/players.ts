export interface Players {
    id: number;
    firstname: string;
    lastname: string;
    teamid: number;
    position: string;
    age: number;
}
