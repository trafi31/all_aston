import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CantiComponent } from './canti.component';

describe('CantiComponent', () => {
  let component: CantiComponent;
  let fixture: ComponentFixture<CantiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CantiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CantiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
